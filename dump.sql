-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Янв 09 2017 г., 14:34
-- Версия сервера: 5.5.52-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `leftist_klika`
--

-- --------------------------------------------------------

--
-- Структура таблицы `genres`
--

CREATE TABLE IF NOT EXISTS `genres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `genre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `genre_index` (`genre`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `genres`
--

INSERT INTO `genres` (`id`, `genre`, `created_date`) VALUES
(1, 'Folk', '2017-01-04 00:00:00'),
(2, 'Rock', '2017-01-04 00:00:00'),
(3, 'Jazz', '2017-01-04 00:00:00'),
(4, 'Blues', '2017-01-04 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `migration_versions`
--

CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migration_versions`
--

INSERT INTO `migration_versions` (`version`) VALUES
('20170108102040'),
('20170108102240');

-- --------------------------------------------------------

--
-- Структура таблицы `singers`
--

CREATE TABLE IF NOT EXISTS `singers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `singer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `singer_index` (`singer`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `singers`
--

INSERT INTO `singers` (`id`, `singer`, `created_date`) VALUES
(1, 'The Kingston Trio', '2017-01-04 00:00:00'),
(2, 'Led Zeppelin', '2017-01-04 00:00:00'),
(3, 'Miles Davis', '2017-01-04 00:00:00'),
(4, 'Mubby Waters', '2017-01-04 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `songs`
--

CREATE TABLE IF NOT EXISTS `songs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `singer_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  `year_id` int(11) NOT NULL,
  `is_active` smallint(6) NOT NULL,
  `created_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `song_index` (`song`),
  KEY `singer_id_index` (`singer_id`),
  KEY `genre_id_index` (`genre_id`),
  KEY `year_id_index` (`year_id`),
  KEY `is_active_index` (`is_active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `songs`
--

INSERT INTO `songs` (`id`, `song`, `singer_id`, `genre_id`, `year_id`, `is_active`, `created_date`, `update_date`) VALUES
(1, 'Tom Dooley', 1, 1, 1, 1, '2017-01-04 00:00:00', '2017-01-04 00:00:00'),
(2, 'Kashmir', 2, 2, 2, 1, '2017-01-04 00:00:00', '2017-01-04 00:00:00'),
(3, 'Blue in Green', 3, 3, 3, 1, '2017-01-04 00:00:00', '2017-01-04 00:00:00'),
(4, 'Mannish Boy', 4, 4, 4, 1, '2017-01-04 00:00:00', '2017-01-04 00:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `years`
--

CREATE TABLE IF NOT EXISTS `years` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `year_index` (`year`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `years`
--

INSERT INTO `years` (`id`, `year`, `created_date`) VALUES
(1, 1958, '0000-00-00 00:00:00'),
(2, 1975, '0000-00-00 00:00:00'),
(3, 1959, '2017-01-04 00:00:00'),
(4, 1955, '2017-01-04 00:00:00');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `songs`
--
ALTER TABLE `songs`
  ADD CONSTRAINT `songs_ibfk_2` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `songs_ibfk_3` FOREIGN KEY (`singer_id`) REFERENCES `singers` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `songs_ibfk_4` FOREIGN KEY (`year_id`) REFERENCES `years` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
