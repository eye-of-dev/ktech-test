<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170108102240 extends AbstractMigration
{

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE songs ADD CONSTRAINT songs_ibfk_2 FOREIGN KEY (genre_id) REFERENCES genres (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE songs ADD CONSTRAINT songs_ibfk_3 FOREIGN KEY (singer_id) REFERENCES singers (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE songs ADD CONSTRAINT songs_ibfk_4 FOREIGN KEY (year_id) REFERENCES years (id) ON UPDATE NO ACTION ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE songs DROP FOREIGN KEY songs_ibfk_2');
        $this->addSql('ALTER TABLE songs DROP FOREIGN KEY songs_ibfk_3');
        $this->addSql('ALTER TABLE songs DROP FOREIGN KEY songs_ibfk_4');
    }

}
