<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170108102040 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE singers (id INT AUTO_INCREMENT NOT NULL, singer VARCHAR(255) NOT NULL, created_date DATETIME NOT NULL, INDEX singer_index (singer), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE genres (id INT AUTO_INCREMENT NOT NULL, genre VARCHAR(255) NOT NULL, created_date DATETIME NOT NULL, INDEX genre_index (genre), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE years (id INT AUTO_INCREMENT NOT NULL, year INT NOT NULL, created_date DATETIME NOT NULL, INDEX year_index (year), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE songs (id INT AUTO_INCREMENT NOT NULL, song VARCHAR(255) NOT NULL, singer_id INT NOT NULL, genre_id INT NOT NULL, year_id INT NOT NULL, is_active SMALLINT NOT NULL, created_date DATETIME NOT NULL, update_date DATETIME NOT NULL, INDEX song_index (song), INDEX singer_id_index (singer_id), INDEX genre_id_index (genre_id), INDEX year_id_index (year_id), INDEX is_active_index (is_active), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE singers');
        $this->addSql('DROP TABLE genres');
        $this->addSql('DROP TABLE years');
        $this->addSql('DROP TABLE songs');
    }
}
