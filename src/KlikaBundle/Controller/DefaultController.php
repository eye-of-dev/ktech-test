<?php

namespace KlikaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    /**
     * @Route("/")
     */
    public function indexAction(Request $request)
    {
        $this->_datatable();
        return $this->render('KlikaBundle:Default:index.html.twig');
    }

    /**
     * Grid action
     * @return Response
     */
    public function gridAction()
    {
        return $this->_datatable()->execute();
    }

    /**
     * set datatable configs
     * @return \Ali\DatatableBundle\Util\Datatable
     */
    private function _datatable()
    {

        return $this->get('datatable')
                        ->setEntity("KlikaBundle:Songs", "songs")
                        ->setFields([
                            "Song" => 'songs.song',
                            "Singer" => 'singer.singer',
                            "Genre" => 'genre.genre',
                            "Year" => 'year.year',
                            "_identifier_" => 'songs.id'
                        ])
                        ->addJoin('songs.singer', 'singer', \Doctrine\ORM\Query\Expr\Join::INNER_JOIN)
                        ->addJoin('songs.genre', 'genre', \Doctrine\ORM\Query\Expr\Join::INNER_JOIN)
                        ->addJoin('songs.year', 'year', \Doctrine\ORM\Query\Expr\Join::INNER_JOIN)
                        ->setWhere('songs.is_active = :is_active', ['is_active' => 1])
                        ->setHasAction(false)
                        ->setSearch(true)
                        ->setSearchFields([1, 2, 3]);
    }

}
