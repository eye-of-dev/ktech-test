<?php

namespace KlikaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Genres
 *
 * @ORM\Table(name="genres", indexes={
 *      @ORM\Index(name="genre_index", columns={"genre"}),
 * })
 * @ORM\Entity(repositoryClass="KlikaBundle\Repository\GenresRepository")
 */
class Genres
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @
     */
    private $genre;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_date;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set genre
     *
     * @param string $genre
     *
     * @return Genres
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * Get genre
     *
     * @return string
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return Genres
     */
    public function setCreatedDate($createdDate)
    {
        $this->created_date = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }
}
