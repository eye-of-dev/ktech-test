<?php

namespace KlikaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Singers
 *
 * @ORM\Table(name="singers", indexes={
 *      @ORM\Index(name="singer_index", columns={"singer"}),
 * })
 * @ORM\Entity(repositoryClass="KlikaBundle\Repository\SingersRepository")
 */
class Singers
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $singer;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_date;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set singer
     *
     * @param string $singer
     *
     * @return Singers
     */
    public function setSinger($singer)
    {
        $this->singer = $singer;

        return $this;
    }

    /**
     * Get singer
     *
     * @return string
     */
    public function getSinger()
    {
        return $this->singer;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     *
     * @return Singers
     */
    public function setCreatedDate($createdDate)
    {
        $this->created_date = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }
}
