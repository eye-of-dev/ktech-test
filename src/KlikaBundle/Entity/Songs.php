<?php

namespace KlikaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Songs
 *
 * @ORM\Table(name="songs", indexes={
 *      @ORM\Index(name="song_index", columns={"song"}),
 *      @ORM\Index(name="singer_id_index", columns={"singer_id"}),
 *      @ORM\Index(name="genre_id_index", columns={"genre_id"}),
 *      @ORM\Index(name="year_id_index", columns={"year_id"}),
 *      @ORM\Index(name="is_active_index", columns={"is_active"}),
 * })
 * @ORM\Entity(repositoryClass="KlikaBundle\Repository\SongsRepository")
 */
class Songs
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @
     */
    private $song;

    /**
     * @ORM\Column(type="integer")
     */
    private $singer_id;

    /**
     * One Song has One Singer.
     * @ORM\OneToOne(targetEntity="KlikaBundle\Entity\Singers")
     * @ORM\JoinColumn(name="singer_id", referencedColumnName="id")
     */
    private $singer;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $genre_id;
    
    /**
     * One Song has One Genre.
     * @ORM\OneToOne(targetEntity="KlikaBundle\Entity\Genres")
     * @ORM\JoinColumn(name="genre_id", referencedColumnName="id")
     */
    private $genre;

    /**
     * @ORM\Column(type="integer")
     */
    private $year_id;
    
    /**
     * One Song has One Year.
     * @ORM\OneToOne(targetEntity="KlikaBundle\Entity\Years")
     * @ORM\JoinColumn(name="year_id", referencedColumnName="id")
     */
    private $year;

    /**
     * @ORM\Column(type="smallint")
     */
    private $is_active;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $update_date;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

}
